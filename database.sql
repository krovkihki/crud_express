create Table person(
    id SERIAL PRIMARY KEY,
    name varchar(255),
    surName varchar(255),
),

create Table post(
    id SERIAL PRIMARY KEY,
    title varchar(255),
    content varchar(255),
    user_id INTEGER(255),
    FOREIGN KEY (user_id) REFERENCES person(id)
)