const Router = require('express');

const router = Router();

const userController = require('../controllers/user-controller');

router.post('/user', userController.createUser);
router.get('/user', userController.getUser);
router.get('/user/:id', userController.getOneUser);
router.put('/user/:id', userController.UpdateUser);
router.delete('/user/:id', userController.deleteUser);

module.exports = router;
