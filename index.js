const express = require('express');
const userRoutes = require('./routes/user-routes');
const postRoutes = require('./routes/post-routes');

const PORT = process.env.PORT || 8000;

const app = express();

app.get('/', (req, res) => {
    res.send('Hello World!!'); //первый метод
});
app.use(express.json());// позволяет express обрабатывать JSON
app.use('/api', userRoutes);// объявляем маршруты
app.use('/api', postRoutes);
// слушаем порт
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
