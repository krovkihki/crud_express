const db = require('../db');

class postController {
    async createPost(request, response) {
        const { title, content, userId } = request.body;
        const newPost = await db.query(
            'INSERT INTO post (title, content, user_id) VALUES ($1,$2,$3) RETURNING *',
            [title, content, userId]
        );
        response.json(newPost.rows[0]);
    }
    async getPost(request, response) {
        const id = request.query.id;
        const posts = await db.query('SELECT * from post where user_id = $1', [
            id,
        ]);
        response.json(posts.rows);
    }
    async deletePost(reqest, response) {
        id = reqest.params.id;
        await db.query('DELETE FROM post WHERE id = $1', [id]);
        response.json('post deleted successfully');
    }
}
module.exports = new postController();
