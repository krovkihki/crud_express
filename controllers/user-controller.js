const db = require('../db');

class UserController {
    async createUser(request, response) {
        const { name, surname } = request.body;
        const newPerson = await db.query(
            'INSERT INTO person (name,surname) VALUES ($1,$2) RETURNING *',
            [name, surname]
        );
        response.json(newPerson.rows[0]);
    }
    async getUser(request, response) {
        const users = await db.query('SELECT * from person');
        response.json(users.rows[0]);
    }
    async getOneUser(request, response) {
        const id = request.params.id;
        const user = await db.query('SELECT * from person WHERE id = $1', [id]);
        response.json(user.rows);
    }
    async UpdateUser(request, response) {
        const id = request.params.id;
        const { name, surname } = request.body;
        await db.query(
            'UPDATE person SET name = $1, surname = $2 WHERE id = $3',
            [name, surname, id]
        );
        response.json('user deleted successfully');
    }
    async deleteUser(request, response) {
        const id = request.params.id;
        await db.query('DELETE FROM person WHERE id = $1', [id]);
        response.json('user deleted successfully');
    }
}
module.exports = new UserController();
